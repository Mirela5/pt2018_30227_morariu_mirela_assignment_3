package TableModels;

import javax.swing.table.AbstractTableModel;

//This abstract class provides default implementations for most of the methods in the TableModel interface.
//It takes care of the management of listeners and provides some conveniences for generating TableModelEvents 
//and dispatching them to the listeners

public class CustomModel extends AbstractTableModel
{
	
	private static final long serialVersionUID = 584852758754803197L;
	private Object[] columnNames;
	private Object[][] data;
	private String objectName;
	
	public CustomModel(Object[] columnNames, Object[][] data) 
	{
		this.columnNames = columnNames;
		this.data = data;				
	}

	@Override
	 public int getColumnCount()
	{
        return columnNames.length;
    }
	
	@Override
    public boolean isCellEditable(int row, int column)
	{ // custom isCellEditable function
        return column != 0;
    }
	
    public int getRowCount() 
    {
        return data.length;
    }

    public String getColumnName(int col)
    {
        return columnNames[col].toString();
    }

    public Object getValueAt(int row, int col) 
    {
        return data[row][col];
    }
    
    public void setValueAt(Object value, int row, int col) 
    {
        data[row][col] = value;
        fireTableCellUpdated(row, col);	// Notifies all listeners that the value of the cell at [row, column] has been updated.
    }
    
    public String getObjectName()
    {
    	return objectName;
    }
    
    public void removeRow(int row)
	{
		for(int i = row + 1; i < data.length; i++)
			data[i-1] = data[i];
		for(int i = 0 ; i < this.getColumnCount(); i++)
			data[data.length - 1][i] = "";
        this.fireTableRowsDeleted(row, row);
	} 
 
}
