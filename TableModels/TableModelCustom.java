package TableModels;

import javax.swing.table.AbstractTableModel;

public class TableModelCustom extends AbstractTableModel
{

	private static final long serialVersionUID = 584852758754803197L;
	private Object[] columnNames;
	private Object[][] data;
	private String objectName;
	
	public TableModelCustom( Object[][] data)
	{
		this.columnNames = new String[4];
		this.columnNames[0] = "Product Id";
		this.columnNames[1] = "Product Name";
		this.columnNames[2] = "Quantity";
		this.columnNames[3] = "Unit Price";
		this.data = data;
	}

	@Override
	 public int getColumnCount()
	{
        return columnNames.length;
    }
	
	@Override
    public boolean isCellEditable(int row, int column)
	{ // custom isCellEditable function
        return column == 2 ;
    }
	
    public int getRowCount()
    {
        return data.length;
    }

    public String getColumnName(int col)
    {
        return columnNames[col].toString();
    }

    public Object getValueAt(int row, int col)
    {
        return data[row][col];
    }
    
    public void setValueAt(Object value, int row, int col)
    {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
    
    public String getObjectName()
    {
    	return objectName;
    }
    
    public void removeRow(int row)
	{
		for(int i = row + 1; i < data.length; i++)
			data[i-1] = data[i];
		for(int i = 0 ; i < this.getColumnCount(); i++)
			data[data.length - 1][i] = "";
        this.fireTableRowsDeleted(row, row);
	}
    
    public void setData(Object[][] data) 
    {
    	this.data = data;
    	this.fireTableRowsInserted(0, data.length);
    }
  
}
