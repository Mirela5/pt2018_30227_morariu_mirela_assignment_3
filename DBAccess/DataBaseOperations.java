package DBAccess;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;

import Domain.Customer;
import Domain.Order;
import Domain.OrderItem;
import Domain.Product;
import Repository.Repository;

public class DataBaseOperations 
{	
		private String jdbcUrl;
		private Connection con;
		public Repository<Product> prodRepo;
		public Repository<Customer> custRepo;
		public Repository<Order> orderRepo;
	
	  public DataBaseOperations()
	  {
		  try 
		  {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		  } 
		  catch (SQLException e) 
		  {			
			e.printStackTrace();
		  
		  }
		  jdbcUrl =  "jdbc:mysql://localhost/ordermanagment?user=root&password=";
		  
		  try 
		  {
			con = DriverManager.getConnection(jdbcUrl);
		  } 
		  catch (SQLException e)
		  {
			e.printStackTrace();
		  }
		  
		 this.getCustomers();
		 this.getProducts();
		 this.getOrder();
		 this.getOrderItem();
		
	  }
	  
	  public void getProducts()
	  {
		  prodRepo = new Repository<>();	
		  try 
		  {
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("select * from Product");
				while (resultSet.next()) 
				{
					prodRepo.addObj(new Product(resultSet.getInt("id"),resultSet.getString("name"),resultSet.getInt("stock"), resultSet.getDouble("price")));
				}
		  } 
		   catch (SQLException e) 
		  {
				
				e.printStackTrace();
		   }
	  }
	  
	  public void getCustomers() 
	  {
		  custRepo = new Repository<>();	
		  try
		  {
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("select * from Customer");
				while (resultSet.next())
				{
					custRepo.addObj(new Customer(resultSet.getInt("id"),resultSet.getString("name"), resultSet.getString("address")));
					
				}
			} 
		  catch (SQLException e) 
		  	{
				
				e.printStackTrace();
			}
	  }	 
	  
	  public void getOrder() 
	  {
		  
		  orderRepo = new Repository<>();	
		  try 
		  {
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("select * from _order");
				while (resultSet.next())
				{
					orderRepo.addObj(new Order(resultSet.getInt("id"),custRepo.getObjById(resultSet.getInt("customer_id")),new Date(resultSet.getLong("date")*1000)));
				}
			} 
		  catch (SQLException e) 
		  {
				e.printStackTrace();
		  }
	  }
	  
	  public void getOrderItem() 
	  {
		  try
		  {
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("select * from orderItem");
				while (resultSet.next()) 
				{
					orderRepo.getObjById(resultSet.getInt("order_id")).addProduct(resultSet.getInt("id"), prodRepo.getObjById(resultSet.getInt("product_id")), resultSet.getInt("quantity") );
				}
			}
		  catch (SQLException e) 
		  {
				
				e.printStackTrace();
		  }
	  }

	  public void insertCustomer(Customer c)
	  {
		  try 
		  {
				Statement statement = con.createStatement();
				String sql ="INSERT INTO `customer` (`id`, `address`, `name`) VALUES ('"+c.id+"', '"+c.getAddress()+"', '"+c.getName()+"')";
				statement.executeUpdate(sql);
				
		   } 
		  catch (SQLException e)
		  {
				e.printStackTrace();
		  }
	  }
	  
	  public void insertProduct(Product p)
	  {
		  try 
		  {
			  	Statement statement = con.createStatement();
				String sql ="INSERT INTO `product` (`id`, `name`, `stock`, `price`) VALUES ('"+p.id+"', '"+p.getName()+"', '"+p.getStock()+"' , '" +p.getPrice() + "')";
				statement.executeUpdate(sql);
		  }
		  catch (SQLException e)
		  {
				e.printStackTrace();
		  }
	  }
	  
	  public void insertOrder(Order o)
	  {
		  try 
		  {
			  	Statement statement = con.createStatement();
				String sql ="INSERT INTO `_order` (`id`, `customer_id`, `date`) VALUES ('"+o.id+"', '"+o.getCustomer().id+"', '"+o.getDate().getTime()/1000+"')";
				System.out.println(sql);
				statement.executeUpdate(sql);
			}
		  catch (SQLException e) 
		  {
				e.printStackTrace();
		  }
	  }
	  public void insertOrderItem(OrderItem oi, int order_id) 
	  {
		  
		  try
		  {
			  	Statement statement = con.createStatement();
				String sql ="INSERT INTO `orderitem` (`id`, `order_id`, `product_id`, `quantity`) VALUES ('"+oi.id+"', '"+order_id+"', '"+oi.getP().id+"', '"+oi.getQuantity()+"')";
				System.out.println(sql);
				statement.executeUpdate(sql);
			} 
		  catch (SQLException e) 
		  	{
				
				e.printStackTrace();
			}
	  }
	  
	  public void updateCustomer(Customer c) 
	  {
		  try 
		  {
				Statement statement = con.createStatement();
				String sql ="UPDATE `customer` SET `address` = '"+c.getAddress()+"', `name` = '"+c.getName()+"' WHERE `customer`.`id` = " + c.id;
				statement.executeUpdate(sql);
				
			} 
		  catch (SQLException e) 
		  {
				
				e.printStackTrace();
			}
	  }
	  
	  public void updateProduct(Product p) 
	  {
		  try 
		  {
			  	Statement statement = con.createStatement();
			  	String sql ="UPDATE `product` SET `name` = '"+p.getName()+"', `stock` = '"+p.getStock()+"', `price` = '"+p.getStock()+"' WHERE `product`.`id` = " + p.id;
				
				statement.executeUpdate(sql);
			} 
		  catch (SQLException e) 
		  {
				e.printStackTrace();
			}
	  }
	  
	 
	  public void deleteCustomer(Customer c)
	  {
		  try 
		  {
				Statement statement = con.createStatement();
				String sql = "DELETE FROM `customer` WHERE `customer`.`id` = " + c.id;
				statement.executeUpdate(sql);
				
				
			}
		  catch (SQLException e)
		  {
				
				e.printStackTrace();
			}
	  }
	  
	  public void deleteProduct(Product p)
	  {
		  try
		  {
			  	Statement statement = con.createStatement();
				String sql = "DELETE FROM `product` WHERE `product`.`id` = " + p.id;
				statement.executeUpdate(sql);
			}
		  catch (SQLException e) 
		  {
				e.printStackTrace();
			}
	  }
	  
	  public int getNextIdCustomer()
	  {
			  
			  try 
			  {
					Statement statement = con.createStatement();
					ResultSet resultSet = statement.executeQuery("SELECT id FROM `customer` ORDER BY id DESC LIMIT 1");
					while (resultSet.next())
					{
						return resultSet.getInt("id") + 1;
						
					}
			  } 
			  catch (SQLException e)
			  {	
					e.printStackTrace();
			  }
			  return 0;
		}
	  
	  public int getNextIdOrderItem() 
	  {
		  
			try 
			{
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT id FROM `orderItem` ORDER BY id DESC LIMIT 1");
				while (resultSet.next()) 
				{
					return resultSet.getInt("id") + 1;	
				}
			} 
			catch (SQLException e)
			{	
				e.printStackTrace();
			}
			return 0;
		}
	  
	  public int getNextIdProduct() 
	  {
		  
			try 
			{
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT id FROM `product` ORDER BY id DESC LIMIT 1");
				while (resultSet.next()) 
				{
					return resultSet.getInt("id") + 1;	
				}
			}
			catch (SQLException e) 
			{	
				e.printStackTrace();
			}
			return 0;
		}
	  
	  public int getNextIdOrder() 
	  {
			 
			try 
			{
				Statement statement = con.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT id FROM `_order` ORDER BY id DESC LIMIT 1");
				while (resultSet.next())
				{
					return resultSet.getInt("id") + 1;	
				}
			} 
			catch (SQLException e)
			{	
				e.printStackTrace();
			}
			return 0;
		}
}
	 
	  

