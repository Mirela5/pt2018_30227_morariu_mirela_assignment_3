package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import Controller.Controller;
import DBAccess.DataBaseOperations;
import Domain.Product;
import TableModels.CustomModel;



public class ListProductsGUI extends JFrame
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5876463756193825983L;
	
	JButton exit;
	JTable table;
	Controller controller;
	public ListProductsGUI(Controller c)
	{
			this.controller = c;
			
			
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("List Products"); 
			this.setVisible(true);
			this.setLayout(null);
			Color customColor1 = new Color(229,38,23);
			Color grey = new Color(229,229,229);
			
			exit = new JButton("BACK");
	        exit.setBounds(570, 380, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);
			
			table = new JTable(Controller.genTable(controller.getProducts()));
			table.setRowHeight(32);
			table.setBackground(grey);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setAlignmentY(CENTER_ALIGNMENT);
			table.getTableHeader().setPreferredSize(new Dimension(32,32));
			table.getModel().addTableModelListener(new ListenerProd());
			JScrollPane tableContainer = new JScrollPane(table);
			tableContainer.setBounds(10,10,685,360);
			this.add(tableContainer);
			
			
			
			
			
			
			
	}
	
	
	
	public static void main(String[] args) 
	{
		 new ListProductsGUI(new Controller(new DataBaseOperations()));
	 }
	private class ListenForButton implements ActionListener
	{			
		
		public void actionPerformed(ActionEvent e) 
		{

		
			if (e.getSource() == exit)
			{
				dispose();
				
				new ProductGUI(controller).setVisible(true);
				
		    }
		}
	

	}

	private class ListenerProd implements TableModelListener {

		@Override
		public void tableChanged(TableModelEvent e) {
			if(e.getType() !=TableModelEvent.DELETE	) {
			int row = e.getFirstRow();
			 
		     CustomModel m = (CustomModel) e.getSource();
		     boolean delete = true;
		     for(int i = 1; i < 4 ; i++) {
			     if(m.getValueAt(row, i).toString().trim().length() != 0)
		    		 delete = false;
		     }
		     if(!delete) {
		    	 int id = (int)m.getValueAt(row, 0);
		    	 String name = m.getValueAt(row, 1).toString();
		    	 int stock;
		    	 double price;
		    	 try {
		    	 stock = Integer.parseInt( m.getValueAt(row, 2).toString());
		    	 price = Double.parseDouble( m.getValueAt(row, 3).toString());
		    	 }catch(NumberFormatException ex) {
		    		 stock = 0;
		    		 price = 0;
		    	 }
		    	 controller.updateProduct(new Product(id,name,stock,price));
		     }
		     else {
		    	 controller.removeProduct(controller.getProduct(row));
		    	 m.removeRow(row);
		     }
		     
			}
		}
    
    }
}

