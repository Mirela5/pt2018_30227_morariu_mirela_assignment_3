package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import Controller.Controller;
import DBAccess.DataBaseOperations;



public class ListOrdersGUI extends JFrame
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5876463756193825983L;
	
	JButton exit;
	JTable table;
	Controller controller;
	public ListOrdersGUI(Controller c)
	{
			this.controller = c;
			
			
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("List Orders"); 
			this.setVisible(true);
			this.setLayout(null);
			Color customColor1 = new Color(229,38,23);
			Color grey = new Color(229,229,229);
			
			exit = new JButton("BACK");
	        exit.setBounds(570, 380, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);
			
			table = new JTable(Controller.genTable(controller.getOrders()));
			table.setRowHeight(32);
			table.setBackground(grey);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setAlignmentY(CENTER_ALIGNMENT);
			table.getTableHeader().setPreferredSize(new Dimension(32,32));
			JScrollPane tableContainer = new JScrollPane(table);
			tableContainer.setBounds(10,10,685,360);
			this.add(tableContainer);
			
			
			
			
			
			
			
	}
	
	
	
	public static void main(String[] args) 
	{
		 new ListOrdersGUI(new Controller(new DataBaseOperations()));
	 }
	private class ListenForButton implements ActionListener
	{			
		
		public void actionPerformed(ActionEvent e) 
		{

		
			if (e.getSource() == exit)
			{
				dispose();
				
				new OrderGUI(controller).setVisible(true);
				
		    }
		}
	

	}

	
}

