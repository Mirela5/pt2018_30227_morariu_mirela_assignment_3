package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import Controller.Controller;
import DBAccess.DataBaseOperations;
import Domain.Customer;
import Domain.Order;
import Domain.OrderItem;
import Domain.Product;
import TableModels.TableModelCustom;


public class OrderGUI extends JFrame
{
	
	private static final long serialVersionUID = -8675880934055495260L;
	JButton exit, list, add;
	JTable customers,products,orderItems;
	Controller controller;
	JScrollPane orderContainer;
	JTextArea textArea;
	int selectedCustomer = -1;					 // default
	ArrayList<OrderItem> orderItemsList;		 // lista cu order items pentru comanda curenta
	
	public OrderGUI(Controller c)
	{
			this.controller = c;
		    this.setSize(720, 720);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("Order"); 
			this.setVisible(true);
			this.setLayout(null);
			
			
			Color customColor1 = new Color(229,38,23);
			Color customColor = new Color(166,189,219);
			Color grey = new Color(255,255,255);
			exit = new JButton("Back");
	        exit.setBounds(570, 620, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);
			
			list = new JButton("List");
			list.setBounds(450, 620, 100, 40);
	        list.setBackground(customColor);
	        list.setVisible(true);
	        this.add(list);	
			ListenForButton forlist = new ListenForButton();
			list.addActionListener(forlist);
			
			add = new JButton("Add");
			add.setBounds(330, 620, 100, 40);
	        add.setBackground(customColor);
	        add.setVisible(true);
	        this.add(add);	
			ListenForButton forAdd = new ListenForButton();
			add.addActionListener(forAdd);
			
			
			customers = new JTable(Controller.genTable(controller.getCustomers()));
			customers.setRowHeight(32);
			customers.setBackground(grey);
			customers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			customers.setAlignmentY(CENTER_ALIGNMENT);
			customers.getTableHeader().setPreferredSize(new Dimension(32,32));
			
			// selecteaza customer-ul pe care s-a apasa
			customers.addMouseListener(new java.awt.event.MouseAdapter()
			{
				public void mouseClicked(java.awt.event.MouseEvent e)
				{
					selectedCustomer = (int) customers.getValueAt(customers.rowAtPoint(e.getPoint()), 0);
					System.out.println(selectedCustomer);	
				}
			});
			
			JScrollPane customersContainer = new JScrollPane(customers);
			customersContainer.setBounds(10,10,680,150);
			this.add(customersContainer);

			// place holder pentr tabel
			orderItemsList= new ArrayList<>(); //pentru fiecare comanda
			Object[][] data = {{"-1", " Add a product", "-", "-"}};
			orderItems = new JTable(new TableModelCustom(data));
			orderItems.setRowHeight(32);
			
			orderItems.setBackground(grey);
			orderItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			orderItems.setAlignmentY(CENTER_ALIGNMENT);
			orderItems.getTableHeader().setPreferredSize(new Dimension(32,32));
			//listener pentru modificat cantitatea
			orderItems.getModel().addTableModelListener(new Listener());
			orderContainer = new JScrollPane(orderItems);
			orderContainer.setBounds(10,350,680,150);
			this.add(orderContainer);
			
			products = new JTable(Controller.genTable(controller.getProducts()));
			products.setRowHeight(32);
			products.setBackground(grey);
			products.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			products.setAlignmentY(CENTER_ALIGNMENT);
			products.getTableHeader().setPreferredSize(new Dimension(32,32));
			// selecteaza + adauga prod in comanda
			products.addMouseListener(new java.awt.event.MouseAdapter()
			{

				public void mouseClicked(java.awt.event.MouseEvent e)
				{
					// ID de prod curent - apasat
					int productID = (int) products.getValueAt(products.rowAtPoint(e.getPoint()), 0);
					Product p = controller.getProductById(productID);
					
					// ID pe care o sa-l aiba orderItem-ul nou creat
					int newID =  controller.getOrderItemNumber();
					OrderItem o = new OrderItem(newID, p ,0);
					orderItemsList.add(o);
					
					// afiseaza prod curente din comanda
					TableModelCustom m = (TableModelCustom) orderItems.getModel();
					m.setData(controller.OrderItemsToMatrix(orderItemsList)); //pune datele in formatul de tabel - trans intr-o matrice de chestii
				}
				
			});
			
			JScrollPane productsContainer = new JScrollPane(products);
			productsContainer.setBounds(10,180,680,150);
			this.add(productsContainer);
			
			// consola pentru afisare mesaje
			textArea = new JTextArea();
			textArea.setBackground(Color.WHITE);
			JScrollPane scrollPane = new JScrollPane(textArea);
		    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setBackground(Color.WHITE);
			this.add(scrollPane);
			scrollPane.setBounds(10,520,680,50);
			
			
	}
	
	//testare
	public static void main(String[] args) 
	{
		 new OrderGUI (new Controller(new DataBaseOperations()));
	}
	
       
	private class ListenForButton implements ActionListener
	{			
		
		public void actionPerformed(ActionEvent e) 
		{

		
			if (e.getSource() == exit)
			{
				dispose();
				try {
					new MainGUI(controller).setVisible(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    }
			if (e.getSource() == list)
			{
				dispose();
				new ListOrdersGUI(controller).setVisible(true);
		    }
			if (e.getSource() == add)
			{
				boolean valuesNotZeroed = true;
				boolean notEnoughStock = false;
				for(OrderItem oi: orderItemsList) {
					if(oi.getQuantity() == 0)
						valuesNotZeroed = false;
					if(oi.getP().getStock() < oi.getQuantity())
						notEnoughStock = true;
						
				}
				if(orderItems.getValueAt(0, 0).toString() == "-1") {
					textArea.append("Select at least a product!\n");
				}
				else if(valuesNotZeroed == false) {
					textArea.append("Quantities should be at least 1!\n");
				}
				else if(selectedCustomer == -1) {
					textArea.append("Please select a customer!\n");
				}
				else if(notEnoughStock) {
					textArea.append("Stock should be bigger than requested quantity\n");
				}
				else{
					Customer c = controller.getCustomerById(selectedCustomer);
					Date date = new Date(Calendar.getInstance().getTime().getTime());
					Order o = new Order(controller.getOrderNumber() + 1, c, date);
					for(OrderItem oi: orderItemsList) {
						o.addProduct(oi.id,oi.getP(),oi.getQuantity());
						oi.getP().setStock(oi.getP().getStock() - oi.getQuantity());
						controller.updateProduct(oi.getP());
					}
					
					controller.insertOrder(o);
					textArea.append("SUCCESS!!!\n");
				}
		    }
		}
	

	}
	private class Listener implements TableModelListener
	{
		@Override
		public void tableChanged(TableModelEvent e) 
		{
			// modificat cantitatea de produs 
			int row = e.getFirstRow();
			System.out.println(orderItems.getValueAt(row, 2).toString());
			orderItemsList.get(row).setQuantity(Integer.parseInt(orderItems.getValueAt(row, 2).toString()));
		}
    
    }
	

}
