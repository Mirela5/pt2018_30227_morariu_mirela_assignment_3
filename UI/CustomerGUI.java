package UI;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import Controller.Controller;
import DBAccess.DataBaseOperations;
import Domain.Customer;
import UI.MainGUI;


public class CustomerGUI extends JFrame
{
	
	private static final long serialVersionUID = 7182819592224280053L;
	
	JButton exit;
	JButton list,add;
	JLabel label, label0, label1, label2;
	JTextField textField1, textField2, textField3;
	JTextArea textArea;
	
	Controller controller;
	public CustomerGUI(Controller c) 
	{
			this.controller = c;
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("Customer"); 
			this.setVisible(true);
			this.setLayout(null);
			
			Color customColor1 = new Color(229,38,23);
			Color customColor = new Color(166,189,219);
			
			exit = new JButton("BACK");
	        exit.setBounds(570, 380, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);

			add = new JButton("ADD");
	        add.setBounds(570, 40, 100, 40);
		    add.setBackground(customColor);
			ListenForButton foradd = new ListenForButton();
			add.addActionListener(foradd);
			this.add(add);
			
			
			
			list = new JButton("LIST");
	        list.setBounds(570, 280, 100, 40);
		    list.setBackground(customColor);
			ListenForButton forlist = new ListenForButton();
			list.addActionListener(forlist);
			this.add(list);
			
			label = new JLabel();
			label.setText("Introduceti datele");
			this.add(label);
			label.setBounds(20, 0, 150, 40);
			
			
			
			
			label1 = new JLabel("Name : ");
			this.add(label1);
			label1.setBounds(20, 160, 50, 20);
			
			textField2 = new JTextField("", 9);
			textField2.requestFocus();
			this.add(textField2);
			textField2.setBounds(100, 160, 150, 20);
			
			label2 = new JLabel("Address : ");
			this.add(label2);
			label2.setBounds(20, 210, 60, 20);
			
			textField3 = new JTextField("", 9);
			textField3.requestFocus();
			this.add(textField3);
			textField3.setBounds(100, 210, 150, 20);
			
			textArea = new JTextArea();
			textArea.setBackground(customColor);
			JScrollPane scrollPane = new JScrollPane(textArea);
		    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setBackground(customColor);
			this.add(scrollPane);
			scrollPane.setBounds(90,300,400,100);
			
			
	}
	
	public void display()
	{
		dispose();
		
		this.setSize(720, 480);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("List customer"); 
		this.setVisible(true);
		this.setLayout(null);
		
	}
	
	public static void main(String[] args) 
	{
		 new CustomerGUI(new Controller(new DataBaseOperations()));
	}
	
	private class ListenForButton implements ActionListener
	{			
		
		public void actionPerformed(ActionEvent e) 
		{

		
			if (e.getSource() == exit)
			{
				dispose();
				
					try {
						new MainGUI(controller).setVisible(true);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
		    }
			
			if (e.getSource() == add)
			{ 
				String name = textField2.getText();
				String address = textField3.getText();
				if(name.isEmpty() || address.isEmpty()) {
					textArea.append("All fields are required\n");
				}
				else {
					controller.insertCustomer(new Customer(controller.db.getNextIdCustomer(),name,address));
					textArea.append("Added successfully\n");
					textField2.setText("");
					textField3.setText("");
				}
				
		    }
			
			
			
			if (e.getSource() == list)
			{
				dispose();
				new ListCustomerGUI(controller).setVisible(true);
		    }
			
			
			
			
		}
	

	}
}
