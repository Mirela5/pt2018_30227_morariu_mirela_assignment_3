package UI;

import javax.swing.*;
//import UI.MainGUI.ListenForButton;

import Controller.Controller;
import DBAccess.DataBaseOperations;

import java.awt.event.*;

//import UI.MainGUI.ListenForButton;

//import UI.GUI.ListenForButton;
import java.awt.Color;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class MainGUI extends JFrame
{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 8416326213728376333L;
	JButton button1,button2,button3;
	JRadioButton list, add, edit, remove;
	JRadioButton addNums, subtractNums, multNums, divideNums, integrateNums;
	JPanel Panel,Panel1,Panel0,Panel3;
	JLabel label1; 
	Controller controller;
	 public MainGUI(Controller c) throws IOException
	 {
		 	this.controller = c;
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setLayout(null);
			this.setTitle("Tema3"); 
		   	this.setVisible(true);
			
		
		    
		   	Color customColor = new Color(166,189,219);
		   	
		 	button1 = new JButton("Customer");
		    button1.setBounds(100, 190, 117, 40);
		 	button1.setBackground(customColor);
		 	ListenForButton forButton1 = new ListenForButton();
			button1.addActionListener(forButton1);
		 	this.add(button1);
			
		    
		    button2 = new JButton("Product");
		    button2.setBounds(280, 190, 117, 40);
		 	button2.setBackground(customColor);
		 	ListenForButton forButton2 = new ListenForButton();
			button2.addActionListener(forButton2);
		    this.add(button2);
		    
		    button3 = new JButton("Order");
		    button3.setBounds(460, 190, 117, 40);
		 	button3.setBackground(customColor);
		 	ListenForButton forButton3 = new ListenForButton();
			button3.addActionListener(forButton3);
		 	this.add(button3);
		 	
			BufferedImage myPicture = ImageIO.read(new File("cs.png"));
		    JLabel picLabel = new JLabel(new ImageIcon(myPicture));
		    this.add(picLabel);
		    picLabel.setBounds(60, 50, 200, 130);
		   	
		    BufferedImage myPicture2 = ImageIO.read(new File("p.png"));
		    JLabel picLabel2 = new JLabel(new ImageIcon(myPicture2));
		    this.add(picLabel2);
		    picLabel2.setBounds(240, 50, 200, 130);
		   	 
		   	BufferedImage myPicture3 = ImageIO.read(new File("order.png"));
		    JLabel picLabel3 = new JLabel(new ImageIcon(myPicture3));
		    this.add(picLabel3);
		    picLabel3.setBounds(420, 50, 200, 130);
		   	 
		   	 

		   	 
		   	
	 }
	 
	 
	 
	 public static void main(String[] args) throws IOException 
	 {
		 new MainGUI (new Controller(new DataBaseOperations()));
	 }
	 
	 private class ListenForButton implements ActionListener
		{			
			
			public void actionPerformed(ActionEvent e) 
			{

			
				if (e.getSource() == button1)
				{
					dispose();
					new CustomerGUI(controller).setVisible(true);
			    }
				
				if (e.getSource() == button2)
				{
					dispose();
					new ProductGUI(controller).setVisible(true);
			    }
				
				if (e.getSource() == button3)
				{
					dispose();
					new OrderGUI(controller).setVisible(true);
			    }
			}
		}
	 
}
	


