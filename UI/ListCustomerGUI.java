package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import Controller.Controller;
import DBAccess.DataBaseOperations;
import Domain.Customer;
import TableModels.CustomModel;



public class ListCustomerGUI extends JFrame
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5876463756193825983L;
	
	JButton exit;
	JTable table;
	Controller controller;
	public ListCustomerGUI(Controller c)
	{
	
			this.controller = c;
		    this.setSize(720, 480);
			this.setLocationRelativeTo(null);
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("List Customers"); 
			this.setVisible(true);
			this.setLayout(null);
			Color customColor1 = new Color(229,38,23);
			Color grey = new Color(229,229,229);
			
			exit = new JButton("BACK");
	        exit.setBounds(570, 380, 100, 40);
		    exit.setBackground(customColor1);
			ListenForButton forexit = new ListenForButton();
			exit.addActionListener(forexit);
			this.add(exit);

			
			table = new JTable(Controller.genTable(controller.getCustomers()));
			table.setRowHeight(32);
			table.setBackground(grey);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setAlignmentY(CENTER_ALIGNMENT);
			table.getTableHeader().setPreferredSize(new Dimension(32,32));
			table.getModel().addTableModelListener(new Listener());
			JScrollPane tableContainer = new JScrollPane(table);
			tableContainer.setBounds(10,10,685,360);
			this.add(tableContainer);
			
			
			
			
			
			
			
	}
	
	
	
	public static void main(String[] args) 
	{
		 new ListCustomerGUI(new Controller(new DataBaseOperations()));
	 }
	private class ListenForButton implements ActionListener
	{			
		
		public void actionPerformed(ActionEvent e) 
		{

		
			if (e.getSource() == exit)
			{
				dispose();
				
				new CustomerGUI(controller).setVisible(true);
				
		    }
		}
	

	}
	private class Listener implements TableModelListener {

		@Override
		public void tableChanged(TableModelEvent e) {
			if(e.getType() !=TableModelEvent.DELETE	) {
			int row = e.getFirstRow(); // randul care tocmai a fost modificat
			 
		     CustomModel m = (CustomModel) e.getSource(); // sursa evenimentului ( tabel)
		     boolean delete = true;
		     for(int i = 1; i < 3 ; i++) {
			     if(m.getValueAt(row, i).toString().trim().length() != 0) // trim - sterge spatiile de la inceputul si sfarsitul stringului
		    		 delete = false;
		     }
		     if(!delete) {
		    	 controller.updateCustomer(new Customer((int)m.getValueAt(row, 0), m.getValueAt(row, 1).toString(), m.getValueAt(row, 2).toString()));
		     }
		     else {
		    	 controller.removeCustomer(new Customer((int)m.getValueAt(row, 0),"",""));
		    	 m.removeRow(row);
		     }
		     
			}
		}
    
    }
}

