package Domain;

import java.util.ArrayList;
import java.util.Date;

public class Order 
{
	public int id;
	private Customer customer;
	private ArrayList<OrderItem> products;
	private Date date;
	
	public Order(int id, Customer c, Date date)
	{
		this.id = id;
		setCustomer(c);
		setProducts(new ArrayList<OrderItem>());
		this.setDate(date);
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void addProduct(int id,Product p, int quantity) 
	{
		getProducts().add(new OrderItem(id,p,quantity));	
	}
	
	public OrderItem getOrderItemById(int id) 
	{
		for(OrderItem i: getProducts())
		{
			if(i.id == id) 
				return i;
		}
		//return null if does not exist;
		return null;
	}
	
	public void removeProduct(OrderItem oi) 
	{
		this.products.remove(oi);
	}

	public Customer getCustomer() 
	{
		return customer;
	}

	public void setCustomer(Customer customer) 
	{
		this.customer = customer;
	}

	public ArrayList<OrderItem> getProducts()
	{
		return products;
	}

	public void setProducts(ArrayList<OrderItem> products) 
	{
		this.products = products;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)   // initializes the object with the current date and time.
	{
		this.date = date;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return (((obj instanceof Order) && (this.id == ((Order) obj).id)) || ((obj instanceof Integer) && (int)obj == this.id));
	}
	
}
