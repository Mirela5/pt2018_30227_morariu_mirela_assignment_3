package Domain;

public class OrderItem
{
	public int id;
	private Product p;
	private int quantity;
	
	public OrderItem (int id,Product p, int quantity)
	{
		this.id = id;
		this.setP(p);
		this.setQuantity(quantity);
	}

	public int getQuantity() 
	{
		return quantity;
	}

	public void setQuantity(int quantity) 
	{
		this.quantity = quantity;
	}

	public Product getP() 
	{
		return p;
	}

	public void setP(Product p) 
	{
		this.p = p;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return obj.equals(id);
	}
	
	public String toString()
	{
		return this.p.getName() + " " + Integer.toString(this.quantity) + " pcs\n";
	}
	
	public String[] toArray()
	{
		// transforma order-item in sir ( formatul necesar pentru table) (un rand al tabelului)
		String[] o = new String[4];
		o[0] = Integer.toString(p.getId());
		o[1] = p.getName();
		o[2] = "0";
		o[3] = Double.toString(p.getPrice());
		return o;
		
	}
}
