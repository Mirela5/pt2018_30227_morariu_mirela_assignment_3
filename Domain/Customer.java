package Domain;

public class Customer 
{
	public int id;
	private String name;
	private String address;
	
	public Customer(int id, String name, String adress) 
	{
		this.id = id;
		this.setName(name);
		this.setAddress(adress);
	}
	
	public String getName() 
	{
		return name;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public String getAddress() 
	{
		return address;
	}
	
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	@Override
	// compara 2 customeri dupa id sau id-ul unui customer cu un numar
	// folosita in repository cand se cauta dupa ID
	public boolean equals(Object obj){
		return (((obj instanceof Customer) && (this.id == ((Customer) obj).id)) || ((obj instanceof Integer) && (int)obj == this.id));
		
	}
	
}
