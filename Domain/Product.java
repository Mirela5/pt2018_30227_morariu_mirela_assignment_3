package Domain;

public class Product 
{
	public int id;
	private String name;
	private int stock;
	private double price;
	
	public Product(int id, String name, int stock, double price) 
	{
		this.id = id;
		this.setName(name);
		this.setStock(stock);
		this.setPrice(price);
	}
	
	public int getId() 
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public int getStock() 
	{
		return stock;
	}

	public void setStock(int stock) 
	{
		this.stock = stock;
	}

	public double getPrice() 
	{
		return price;
	}

	public void setPrice(double price) 
	{
		this.price = price;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return (((obj instanceof Product) && (this.id == ((Product) obj).id)) || ((obj instanceof Integer) && (int)obj == this.id));
	}
	
}
