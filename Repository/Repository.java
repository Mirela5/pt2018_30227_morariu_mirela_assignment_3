package Repository;

import java.util.ArrayList;

// salveaza datele local
// t - generic type
public class Repository<T>
{
	private ArrayList<T> objList;
	
	public Repository()
	{
		objList = new ArrayList<>();
	}
	
	public int getLen() 
	{
		return objList.size();
	}
	
	public T getObj(int pos) 
	{
		return objList.get(pos);
	}
	
	public void addObj(T obj) 
	{
		objList.add(obj);
	}
	
	public void removeObj(T obj) 
	{
		objList.remove(this.getPos(obj));
	}
	
	public void updateObj(T newObj)
	{
		objList.set(this.getPos(newObj), newObj);
	}
	
	public int getPos(T obj) 
	{
		for(int i = 0 ; i < objList.size(); i++) 
		{
			if(objList.get(i).equals(obj))
			{
				return i;
			}
		}
		// returns -1 if not found
		return -1;
	}
	
	public T getObjById(int id)
	{
		for(T i: objList) 
		{
			if(i.equals(id))
				return i;
		}
		return null;
	
	}
	public ArrayList<T> getObjs()
	{
		return objList;
	}	
	
}
