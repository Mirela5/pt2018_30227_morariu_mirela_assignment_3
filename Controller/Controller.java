package Controller;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import DBAccess.DataBaseOperations;
import Domain.Customer;
import Domain.Order;
import Domain.OrderItem;
import Domain.Product;
import TableModels.CustomModel;

public class Controller {
	public DataBaseOperations db;
	
	public Controller(DataBaseOperations db){
		this.db = db;
	}

	
	public void insertProduct(Product p) {
		db.prodRepo.addObj(p);
		db.insertProduct(p);
	}

	public void insertOrder(Order o) {
		db.orderRepo.addObj(o);
		db.insertOrder(o);
		// local - order e un singur obiect cu referinte la order item
		// DB - orderitem este stocat separat de order 
		for(OrderItem oi: o.getProducts()) {
			this.insertOrderItem(oi, o.id);
			System.out.println(oi.id);
		}
	}
	
	public void insertOrderItem(OrderItem oi, int order_id) {
		
			db.insertOrderItem(oi, order_id );
			
	}
	
	public void insertCustomer(Customer c) {
		db.custRepo.addObj(c);
		db.insertCustomer(c);
	}
	
	public void updateProduct(Product p) {
		db.prodRepo.updateObj(p);
		db.updateProduct(p);
	}

	
	
	public void updateCustomer(Customer c) {
		db.custRepo.updateObj(c);
		db.updateCustomer(c);
	}
	
	public void removeProduct(Product p) {
		db.prodRepo.removeObj(p);
		db.deleteProduct(p);
	}

	
	
	public void removeCustomer(Customer c) {
		db.custRepo.removeObj(c);
		db.deleteCustomer(c);
	}

	public int getCustomerNumber() {
		return db.custRepo.getLen();
	}
	
	
	public Customer getCustomer(int pos) {
		return db.custRepo.getObj(pos);
	}

	public int getOrderNumber() {
		return db.orderRepo.getLen();
	}
	
	
	public Order getOrder(int pos) {
		return db.orderRepo.getObj(pos);
	}
	
	public int getProductNumber() {
		return db.prodRepo.getLen();
	}
	
	
	public Product getProduct(int pos) {
		return db.prodRepo.getObj(pos);
	}
	
	public ArrayList<Customer> getCustomers() {
		return db.custRepo.getObjs();
	}
	public ArrayList<Product> getProducts() {
		return db.prodRepo.getObjs();
	}
	public ArrayList<Order> getOrders() {
		return db.orderRepo.getObjs();
	}
	
	public static <T> CustomModel genTable(ArrayList<T> objs) {

		List<String> fieldList = new ArrayList<>();//numele coloanelor
		// declarare default
		Object[][] data = new Object[1][1];
		
		if(objs.size() > 0) {
			// objs.get(0) - primul obiect din lista
			// .getClass() - indentifica clasa obiectului
			// .getDeclaredFields() - cere lista tuturor campurilor ( numele coloanelor)
			Field[] fields = objs.get(0).getClass().getDeclaredFields();
			
			for(Field f : fields) {
				String[] fs = f.toString().split(Pattern.quote("."));
				// primul caracter cu litera mare restul cu litera mica
				fieldList.add(fs[fs.length - 1].substring(0, 1).toUpperCase() + fs[fs.length - 1].substring(1));
			}
			
			// --- end generare cap tabel
			//  |  |  |
			// \ /\ /\ / generare continut tabel
			// instantiere matrice cu numarul de coloane si randuri ale tabelului
			data = new Object[objs.size()][fieldList.size()];
			// counter randuri
			int j = 0;
			// parcurgem lista de obiecte
			for(T obj : objs) {
				for(int i = 0; i < fieldList.size(); i++) {
					try {
						// apelam get-erele fiecarui camp
						data[j][i] = obj.getClass().getMethod("get"+fieldList.get(i)).invoke(obj);
						// prindem toate exceptiile posibile
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException |NoSuchMethodException | SecurityException e) {
						
						e.printStackTrace();
					}
				}
				// incrementare counter
				j++;
			}

			
		}
		// returnam modelul tabelului in urma folosirii reflection technique
		return new CustomModel(fieldList.toArray(),data);
	}
	public int getOrderItemNumber() {
		int number = 0;
		for(Order o: db.orderRepo.getObjs()) {
			for(OrderItem oi: o.getProducts()) {
				if(oi.id > number)
					number = oi.id;
			}
		}
		return number + 1;
	}
	public ArrayList<OrderItem> getOrderItems(int orderId) {
		return db.orderRepo.getObjById(orderId).getProducts();
	}
	public Product getProductById(int id) {
		return db.prodRepo.getObjById(id);
	}
	
	public Object[][] OrderItemsToMatrix(ArrayList<OrderItem> orderItems) {
		Object[][] matrix= new Object[orderItems.size()][4];
		for(int i = 0 ; i < orderItems.size(); i++) {
			matrix[i] = orderItems.get(i).toArray();
		}
		return matrix;
	}


	public Customer getCustomerById(int id) {
		
		return this.db.custRepo.getObjById(id);
	}
	
	
	
	
}
